//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.ContextHierarchy;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import javax.annotation.Resource;
//
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
///**
// * Created with IntelliJ IDEA.
// * User: Sebastian MA
// * Date: March 30, 2014
// * Time: 15:51
// */
//@RunWith(SpringJUnit4ClassRunner.class)
//@WebAppConfiguration(value = "src/main/webapp")
//@ContextHierarchy({
//		@ContextConfiguration(name = "parent",
//				locations = "classpath:/spring/*context.xml"),
//		@ContextConfiguration(name = "child",
//				locations = "classpath:/spring/dispatcher-servlet.xml")
//})
//public class ControllerTest {
//
//	@Resource
//	private WebApplicationContext applicationContext;
//
//	private MockMvc mockMvc;
//
//	@Before
//	public void setUp() {
//
//		mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
//	}
//
//	@Test
//	public void testSignIn() throws Exception {
//
//
//		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/misc/test"))
//		                          .andDo(MockMvcResultHandlers.print())
//		                          .andExpect(status().isOk())
//		                          .andReturn();
//
//
//	}
//
//
//}

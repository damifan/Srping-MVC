var app = angular.module('app', []);
app.controller('controller', function ($scope, $http, $location) {

    //用户CURD
    $scope.model = DataResolver($http, {
        contextPath: contextPath,
        servicePath: "chargeUserMgt",
        add: {
            url: "add",
            begin: function () {
                $('#modal-add').modal('show');
            },
            success: function () {
                $('#modal-add').modal('hide');
            },
            error: function () {
            },
            validate: function (model, validationResult) {
                console.log("Validating ", model);
                return true;
            }

        },
        remove: {
            url: "remove",
            begin: function () {
                $('#modal-remove').modal('show');
            },
            success: function () {
                $('#modal-remove').modal('hide');
            },
            error: function () {
            }
        },
        update: {
            url: "update",
            begin: function () {
                $('#modal-update').modal('show');
            },
            success: function () {
                $('#modal-update').modal('hide');
            },
            error: function () {
            },
            validate: function (model, validationResult) {
                console.log("Validating ", model);
                return true;
            }
        },
        query: {
            url: "query",
            begin: function () {
            },
            success: function () {
            },
            error: function () {
            }
        },
        details: {
            url: "details",
            begin: function (model) {
                $scope.user = angular.copy(model);
                $('#modal-details').modal('show');
            },
            success: function () {
                $('#modal-details').modal('hide');
            },
            error: function () {
            }
        },
        validatorModelName: 'validationResult'

    });

    $scope.model.query.commit();

    //订单增加dialog 省市区
    $scope.provinceOption = '';
    $scope.cityOption = '';
    $scope.districtOption = '';

    //请求省市区列表
    $http({
        method: 'POST',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        url: url("/address/getProvince")
    }).success(function (data, status) {
        console.log("provinceOption ", data);
        $scope.provinceOptions = angular.copy(data);
    });

    //省下拉监听
    $scope.provinceChange = function (provinceId) {
        if (provinceId != '' && provinceId != null) {
            $scope.selectGetCity(provinceId);
        } else {
            $scope.cityOptions = '';
        }
    }

    //市下拉监听
    $scope.cityChange = function (cityId) {
        if (cityId != '' && cityId != null) {
            $scope.selectGetDistrict(cityId);
        } else {
            $scope.districtOptions = '';
        }
    }

    //请求城市列表
    $scope.selectGetCity = function (provinceId) {
        $http({
            method: 'POST',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url: url("/address/getCity"),
            data: $.param({provinceId: provinceId})
        }).success(function (data) {
            console.log("selectGetCity ", data);
            $scope.cityOptions = angular.copy(data);
        });
    }

    //请求区县列表
    $scope.selectGetDistrict = function (cityId) {
        $http({
            method: 'POST',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            url: url("/address/getDistrict"),
            data: $.param({cityId: cityId})
        }).success(function (data) {
            console.log("selectGetDistrict ", data);
            $scope.districtOptions = angular.copy(data);
        });
    }


});







package com.demo.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

public class AbstractSpringBean {

    protected static Logger log;

    @PostConstruct
    protected void postConstruct() {

        log = LoggerFactory.getLogger(this.getClass().getSimpleName());
    }

}

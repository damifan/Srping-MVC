package com.demo.domain;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Wang Lei
 * Date: 2015/3/31
 * Time: 19:33
 * <p/>
 * TODO
 */
@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String username;

    @Column(name = "password")
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

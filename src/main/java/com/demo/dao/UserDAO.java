package com.demo.dao;

import com.demo.domain.UserEntity;
import org.springframework.stereotype.Repository;
import org.triiskelion.tinyspring.dao.AbstractDao;

/**
 * Created with IntelliJ IDEA.
 * User: Wang Lei
 * Date: 2015/3/31
 * Time: 19:52
 * <p/>
 * TODO
 */
@Repository
public class UserDAO extends AbstractDao<UserEntity> {
    @Override
    protected Class<UserEntity> getEntityClass() {
        return UserEntity.class;
    }
}

package com.demo.controller;

import com.demo.base.AbstractSpringBean;
import com.demo.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * User: Wang Lei
 * Date: 2015/3/31
 * Time: 16:13
 * <p/>
 * TODO
 */
@RequestMapping(value = "user")
@Controller
public class UserController extends AbstractSpringBean {

    @Resource
    private UserService userService;

    /**
     * 跳转到登录页
     *
     * @return
     */
    @RequestMapping("index")
    public String index() {

        return "login";
    }

    /**
     * 跳转到注册页
     *
     * @return
     */
    @RequestMapping("toRegister")
    public String toRegister() {

        return "register";
    }

    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("login")
    @ResponseBody
    public String login(@RequestParam String username, @RequestParam(value = "password") String password) {
        if (userService.login(username, password)) {
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }

    /**
     * 注册
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("register")
    @ResponseBody
    public String register(@RequestParam String username, @RequestParam(value = "password") String password) {
        if (userService.register(username, password)) {
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }
}
